import { calcDrawArea } from "./calcDrawArea";

const canvas: HTMLCanvasElement = document.getElementById(
  "canvas"
) as HTMLCanvasElement;
const ctx = canvas.getContext("2d")!;
const video: HTMLVideoElement = document.getElementById(
  "video"
) as HTMLVideoElement;

const videoWidth = 600;
const videoHeight = 400;
navigator.mediaDevices
  .getUserMedia({
    video: {
      width: videoWidth,
      height: videoHeight
    },
    audio: false
  })
  .then(stream => {
    return new Promise(resolve => {
      video.srcObject = stream;
    });
  });

canvas.width = 640;
canvas.height = 360;
const a = calcDrawArea(canvas.width, canvas.height, videoWidth, videoHeight);
console.log(JSON.stringify(a));

function draw() {
  // drawImage(image, srcX: number, srcY: number, srcW: number, srcH: number, dstX: number, dstY: number, dstW: number, dstH: number): void;
  ctx.drawImage(
    video,
    a.srcX,
    a.srcY,
    a.srcWidth,
    a.srcHeight,
    a.destX,
    a.destY,
    a.destWidth,
    a.destHeight
  );
}

function render(time: number) {
  draw();
  requestAnimationFrame(render);
}
requestAnimationFrame(render);
