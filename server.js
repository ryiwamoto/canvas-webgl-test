var https = require("https");
var fs = require("fs");
var express = require("express");

var ssl_server_key = "server_key.pem";
var ssl_server_crt = "server_crt.pem";

var options = {
  key: fs.readFileSync(ssl_server_key),
  cert: fs.readFileSync(ssl_server_crt),
  requestCert: false,
  rejectUnauthorized: false
};

var app = express();
app.use(express.static("dist"));

var server = https.createServer(options, app).listen(3000, function() {
  console.log("server started at port 3000");
});
