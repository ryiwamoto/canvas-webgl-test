/**
 * 描画領域
 */
export interface DrawArea {
  srcX: number;
  srcY: number;
  srcWidth: number;
  srcHeight: number;
  destX: number;
  destY: number;
  destWidth: number;
  destHeight: number;
}

/**
 * 描画領域に収まるように切り取りを考慮して映像の描画を計算する
 * @param canvasWidth canvasの幅(px)
 * @param canvasHeight canvasの高さ(px)
 * @param sourceWidth ソース画像の幅(px)
 * @param sourceHeight ソース画像の高さ(px)
 */
export function calcDrawArea(
  canvasWidth: number,
  canvasHeight: number,
  sourceWidth: number,
  sourceHeight: number
): DrawArea {
  if (canvasWidth / canvasHeight > sourceWidth / sourceHeight) {
    const scale = canvasWidth / sourceWidth;
    return {
      srcX: 0,
      srcY: 0,
      srcWidth: sourceWidth,
      srcHeight: sourceHeight,
      destWidth: Math.round(sourceWidth * scale),
      destHeight: Math.round(sourceHeight * scale),
      destX: 0,
      destY: Math.round((canvasHeight - sourceHeight * scale) / 2)
    };
  } else {
    const scale = canvasHeight / sourceHeight;
    return {
      srcX: 0,
      srcY: 0,
      srcWidth: sourceWidth,
      srcHeight: sourceHeight,
      destWidth: Math.round(sourceWidth * scale),
      destHeight: Math.round(sourceHeight * scale),
      destX: Math.round((canvasWidth - sourceWidth * scale) / 2),
      destY: 0
    };
  }
}
