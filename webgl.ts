import w from "./webgl-utils";
import m from "./m4";
import { calcDrawArea } from "./calcDrawArea";

const m4 = m.m4;
const webglUtils = w.webglUtils;

const canvas: HTMLCanvasElement = document.getElementById(
  "canvas"
) as HTMLCanvasElement;
const video: HTMLVideoElement = document.getElementById(
  "video"
) as HTMLVideoElement;

const gl = canvas.getContext("webgl")!;
if (!gl) {
  throw new Error("webgl is not supported");
}

type TextureInfo = {
  width: number;
  height: number;
  texture: WebGLTexture;
};

function loadCameraTexture(width: number, height: number): TextureInfo {
  const tex = gl.createTexture()!;
  gl.bindTexture(gl.TEXTURE_2D, tex);
  // Fill the texture with a 1x1 blue pixel.
  gl.texImage2D(
    gl.TEXTURE_2D,
    0,
    gl.RGBA,
    1,
    1,
    0,
    gl.RGBA,
    gl.UNSIGNED_BYTE,
    new Uint8Array([0, 0, 255, 255])
  );

  // let's assume all images are not a power of 2
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  const textureInfo = {
    width: 1,
    height: 1,
    texture: tex
  };
  navigator.mediaDevices
    .getUserMedia({
      video: {
        width,
        height
      },
      audio: false
    })
    .then(stream => {
      return new Promise(resolve => {
        video.srcObject = stream;
        video.addEventListener("loadedmetadata", () => {
          textureInfo.width = video.videoWidth;
          textureInfo.height = video.videoHeight;
          gl.bindTexture(gl.TEXTURE_2D, textureInfo.texture);
          gl.texImage2D(
            gl.TEXTURE_2D,
            0,
            gl.RGBA,
            gl.RGBA,
            gl.UNSIGNED_BYTE,
            video
          );
        });
      });
    });
  return textureInfo;
}

webglUtils.resizeCanvasToDisplaySize(gl.canvas);
// Tell WebGL how to convert from clip space to pixels
gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

// setup GLSL program
const program = webglUtils.createProgramFromScripts(gl, [
  "drawImage-vertex-shader",
  "drawImage-fragment-shader"
]);

// look up where the vertex data needs to go.
const positionLocation = gl.getAttribLocation(program, "a_position");
const texcoordLocation = gl.getAttribLocation(program, "a_texcoord");

// lookup uniforms
const matrixLocation = gl.getUniformLocation(program, "u_matrix")!;
const textureMatrixLocation = gl.getUniformLocation(
  program,
  "u_textureMatrix"
)!;
const textureLocation = gl.getUniformLocation(program, "u_texture")!;

// Create a buffer.
const positionBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

// Put a unit quad in the buffer
const positions = [0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1];
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

// Create a buffer for texture coords
const texcoordBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

// Put texcoords in the buffer
const texcoords = [0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1];
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texcoords), gl.STATIC_DRAW);

type drawInfo = {
  x: number;
  y: number;
  dx: number;
  dy: number;
  textureInfo: TextureInfo;
};

const videoWidth = 1200;
const videoHeight = 800;
let drawInfo: drawInfo = {
  x: 0,
  y: 0,
  dx: 0,
  dy: 0,
  textureInfo: loadCameraTexture(videoWidth, videoHeight)
};

const a = calcDrawArea(canvas.width, canvas.height, videoWidth, videoHeight);
bindImage(
  a.srcWidth,
  a.srcHeight,
  a.srcX,
  a.srcY,
  a.srcWidth,
  a.srcHeight,
  a.destX,
  a.destY,
  a.destWidth,
  a.destHeight
);

function draw() {
  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.bindTexture(gl.TEXTURE_2D, drawInfo.textureInfo.texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, video);
  drawImage(drawInfo.textureInfo.texture);
}

function render(time: number) {
  draw();
  requestAnimationFrame(render);
}
requestAnimationFrame(render);

function bindImage(
  texWidth: number,
  texHeight: number,
  srcX: number,
  srcY: number,
  srcWidth: number,
  srcHeight: number,
  dstX: number,
  dstY: number,
  dstWidth: number,
  dstHeight: number
) {
  // Tell WebGL to use our shader program pair
  gl.useProgram(program);

  // Setup the attributes to pull data from our buffers
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.enableVertexAttribArray(positionLocation);
  gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);
  gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
  gl.enableVertexAttribArray(texcoordLocation);
  gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);

  // this matirx will convert from pixels to clip space
  var matrix = m4.orthographic(0, gl.canvas.width, gl.canvas.height, 0, -1, 1);

  // this matrix will translate our quad to dstX, dstY
  matrix = m4.translate(matrix, dstX, dstY, 0);

  // this matrix will scale our 1 unit quad
  // from 1 unit to texWidth, texHeight units
  matrix = m4.scale(matrix, dstWidth, dstHeight, 1);

  // Set the matrix.
  gl.uniformMatrix4fv(matrixLocation, false, matrix);

  // Because texture coordinates go from 0 to 1
  // and because our texture coordinates are already a unit quad
  // we can select an area of the texture by scaling the unit quad
  // down
  var texMatrix = m4.translation(srcX / texWidth, srcY / texHeight, 0);
  texMatrix = m4.scale(
    texMatrix,
    srcWidth / texWidth,
    srcHeight / texHeight,
    1
  );

  // Set the texture matrix.
  gl.uniformMatrix4fv(textureMatrixLocation, false, texMatrix);

  // Tell the shader to get the texture from texture unit 0
  gl.uniform1i(textureLocation, 0);
}

function drawImage(tex: WebGLTexture) {
  gl.bindTexture(gl.TEXTURE_2D, tex);
  gl.drawArrays(gl.TRIANGLES, 0, 6);
}
